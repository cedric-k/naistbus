package com.dexterlab.cedric_k.busnaist;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.AsyncTask;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Timer;
import java.util.TimerTask;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {



    String originvalue, destinationvalue = "";
    TextView minuteremaining ;
    TextView exact;
    Button send;
    TextView headtitle;
    TextView at;
    TextView fullscheduleheader;
    int remaining;
    String fulltab;
    String exacttime;
    String fare, infodate;
    Timer timer;
    TimerTask timerTask;
    ListView fullschedule;
    TextView checkinternet;
    TextView displayfare;
    TextView txtinfodate;

    ArrayList<String> listItems=new ArrayList<String>();
    ArrayAdapter<String> ada;

    final Handler handler = new Handler();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

         final String[] places = new String[]{
                String.valueOf(getText(R.string.naist)),String.valueOf(getText(R.string.gakkennaratomigaoka)), String.valueOf(getText(R.string.gakkenkitaikoma)), String.valueOf(getText(R.string.gakuenmae)), String.valueOf(getText(R.string.takanohara)),String.valueOf(getText(R.string.chiku))
        };

         final String[] placeA = new String[]{
                 String.valueOf(getText(R.string.naist))
        };

        final String[] placeB = new String[]{
                String.valueOf(getText(R.string.gakuenmae))
        };

        final String[] placeC = new String[]{
                String.valueOf(getText(R.string.gakkenkitaikoma)), String.valueOf(getText(R.string.gakuenmae)), String.valueOf(getText(R.string.gakkennaratomigaoka)),String.valueOf(getText(R.string.takanohara))
        };

        final String[] placeD = new String[]{
                String.valueOf(getText(R.string.naist)),String.valueOf(getText(R.string.chiku))
        };

        ada=new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                listItems);






        send = (Button) findViewById(R.id.buttonsend);
        minuteremaining = (TextView) findViewById(R.id.minremaining);
        exact = (TextView) findViewById(R.id.exact);
        fullschedule= (ListView) findViewById(R.id.fullschedule);
        fullscheduleheader= (TextView) findViewById(R.id.fullscheduleheader);
        headtitle = (TextView) findViewById(R.id.headtitle);
        txtinfodate = (TextView) findViewById(R.id.infodate);
        displayfare = (TextView) findViewById(R.id.fare);
        at = (TextView) findViewById(R.id.at);
        checkinternet = (TextView) findViewById(R.id.checkinternet);
        fullschedule.setAdapter(ada);



        //adapter settings

        ArrayAdapter<String> adapter_spin = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, places);
        adapter_spin.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        final ArrayAdapter<String> adapter_spinA = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, placeA);
        adapter_spinA.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        final ArrayAdapter<String> adapter_spinB = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, placeB);
        adapter_spinB.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        final ArrayAdapter<String> adapter_spinC = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, placeC);
        adapter_spinC.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        final ArrayAdapter<String> adapter_spinD = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, placeD);
        adapter_spinD.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


        final Spinner origin = (Spinner)
                findViewById(R.id.origin);
        final Spinner destination = (Spinner)
                findViewById(R.id.destination);

        origin.setAdapter(adapter_spin);
        destination.setAdapter(adapter_spin);

        origin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                origin.setSelection(position);
                originvalue = (String) origin.getSelectedItem();
                if (originvalue==String.valueOf(getText(R.string.naist))) {
                    destination.setAdapter(adapter_spinC);
                    originvalue="Naist";
                }else if (originvalue==String.valueOf(getText(R.string.gakuenmae))){
                    destination.setAdapter(adapter_spinD);
                    originvalue="Gakuenmae";
                }else if (originvalue==String.valueOf(getText(R.string.chiku))){
                    destination.setAdapter(adapter_spinB);
                    originvalue="Chiku-center(Gourmet-city)";
                }else if (originvalue==String.valueOf(getText(R.string.takanohara))){
                    originvalue="Takanohara";
                    destination.setAdapter(adapter_spinA);
                }else if (originvalue==String.valueOf(getText(R.string.gakkenkitaikoma))){
                    originvalue="Gakkenkitaikoma";
                    destination.setAdapter(adapter_spinA);
                }else if (originvalue==String.valueOf(getText(R.string.gakkennaratomigaoka))){
                    originvalue="Aeon-Tomigaoka";
                    destination.setAdapter(adapter_spinA);
                }
                else{
                    destination.setAdapter(adapter_spinA);
                }
                //Toast.makeText(getBaseContext(),originvalue, Toast.LENGTH_LONG).show();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        destination.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                destination.setSelection(position);
                destinationvalue = (String) destination.getSelectedItem();

                if(destinationvalue.equals(String.valueOf(getText(R.string.naist)))) {
                    destinationvalue = "Naist";
                }
                if(destinationvalue.equals(String.valueOf(getText(R.string.gakuenmae)))) {
                    destinationvalue = "Gakuenmae";
                }
                if(destinationvalue.equals(String.valueOf(getText(R.string.chiku)))) {
                    destinationvalue = "Chiku-center(Gourmet-city)";
                }
                if(destinationvalue.equals(String.valueOf(getText(R.string.gakkenkitaikoma)))) {
                    destinationvalue = "Gakkenkitaikoma";
                }
                if(destinationvalue.equals(String.valueOf(getText(R.string.takanohara)))) {
                    destinationvalue = "Takanohara";
                }
                if(destinationvalue.equals(String.valueOf(getText(R.string.gakkennaratomigaoka)))) {
                    destinationvalue = "Aeon-Tomigaoka";
                }
                //Toast.makeText(getBaseContext(), destinationvalue, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //originvalue = origin.getText().toString();
                //destinationvalue = destination.getText().toString();
                timer.cancel();



                //Toast.makeText(getBaseContext(), "http://ubi-lab.com/~cedric-k/busschedule/busschedulechecker.php?from=" + originvalue + "&to=" + destinationvalue, Toast.LENGTH_LONG).show();
                new HttpAsyncTask().execute("http://ubi-lab.com/~cedric-k/busschedule/busschedulechecker.php?from=" + originvalue + "&to=" + destinationvalue);
            send.setEnabled(false);
            }
        });


        fullschedule.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Toast.makeText(MainActivity.this,fullschedule.getItemAtPosition(i).toString(),Toast.LENGTH_LONG).show();
                String sttime=fullschedule.getItemAtPosition(i).toString();

                String[] h=sttime.split(":");

                SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
                String dateInString = "22-01-2015 10:20:56";

                Long actualtime=new GregorianCalendar().getTimeInMillis();

                long rawtimeinmilli= (Long.valueOf(h[0])*60+Long.valueOf(h[1]))*60*1000;

                long wishedtimeinmilli=actualtime+rawtimeinmilli;

                //long finaltime= wishedtimeinmilli-actualtime;

                    scheduleAlarm(view,wishedtimeinmilli);
            }
        });





    }

    @Override
    protected void onResume() {
        super.onResume();
//Toast.makeText(this,"trttr",Toast.LENGTH_LONG).show();
        stoptimertask();
        //Toast.makeText(getBaseContext(), "http://ubi-lab.com/~cedric-k/busschedule/busschedulechecker.php?from=" + originvalue + "&to=" + destinationvalue, Toast.LENGTH_LONG).show();
        new HttpAsyncTask().execute("http://ubi-lab.com/~cedric-k/busschedule/busschedulechecker.php?from=" + originvalue + "&to=" + destinationvalue);
        send.setEnabled(false);
    }
    protected void onStop(){
        super.onStop();
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }
    protected void onPause(){
        super.onPause();
        if (timer != null) {
            timer.cancel();
            timer = null;
        }

    }
    protected void onDestroy(){
        super.onDestroy();
        if (timer != null) {
            timer.cancel();
            timer = null;
        }

    }

    public void scheduleAlarm(View V,Long time){

        //time = new GregorianCalendar().getTimeInMillis()+1*1*30*1000;
        Intent intentAlarm = new Intent(this, Alarmreceiver.class);
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP,time, PendingIntent.getBroadcast(this,1,  intentAlarm, PendingIntent.FLAG_UPDATE_CURRENT));
        Toast.makeText(this, "Alarm set for xx:xx", Toast.LENGTH_LONG).show();

    }






    public void startTimer() {
        //set a new Timer
        timer = new Timer();

        //initialize the TimerTask's job
        initializeTimerTask();

        //schedule the timer, after the first 5000ms the TimerTask will run every 1000ms
        timer.schedule(timerTask, 1000, 1000); //
    }

    public void updateadapter() {
        //adapter.notifyDataSetChanged();
    }

    public void stoptimertask() {
        //stop the timer, if it's not already null
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    public void initializeTimerTask() {

        timerTask = new TimerTask() {
            public void run() {

                //use a handler to run a toast that shows the current timestamp
                handler.post(new Runnable() {
                    public void run() {

                        //Toast.makeText(getBaseContext(), "in hand", Toast.LENGTH_LONG).show();
                        //if(originvalue!="Naist" && destinationvalue!="Naist") {
                            //Toast.makeText(getBaseContext(), "in if", Toast.LENGTH_LONG).show();
                        boolean internet=isNetworkAvailable();
                        if(!internet)
                            checkinternet.setText(String.valueOf(getText(R.string.internet_check)));
                        else
                        checkinternet.setText("");
                        if(remaining>0) {
                            remaining = remaining - 1;
                            minuteremaining.setText(convertsecondintomin(remaining).toString());

                            if (remaining <= 1) {

                          /*
                                if(originvalue=="奈良先端科学技術大学院大学")
                                    originvalue="Naist";
                                if(originvalue=="学園前")
                                    originvalue="Gakuenmae";
                                if(originvalue=="地区センター")
                                    originvalue="Chiku-center(Gourmet-city)";
                                if(originvalue=="学研北生駒")
                                    originvalue="Gakkenkitaikoma";
                                if(originvalue=="高の原")
                                    originvalue="Takanohara";

                                if(destinationvalue=="奈良先端科学技術大学院大学")
                                    destinationvalue="Naist";
                                if(destinationvalue=="学園前")
                                    destinationvalue="Gakuenmae";
                                if(destinationvalue=="地区センター")
                                    destinationvalue="Chiku-center(Gourmet-city)";
                                if(destinationvalue=="学研北生駒")
                                    destinationvalue="Gakkenkitaikoma";
                                if(destinationvalue=="高の原")
                                    destinationvalue="Takanohara";
                                */


                                timer.cancel();
                                new HttpAsyncTask().execute("http://ubi-lab.com/~cedric-k/busschedule/busschedulechecker.php?from=" + originvalue + "&to=" + destinationvalue);
                                //Toast.makeText(getBaseContext(), "http://ubi-lab.com/~cedric-k/busschedule/busschedulechecker.php?from=" + originvalue + "&to=" + destinationvalue, Toast.LENGTH_LONG).show();
                            }
                        }
                       // }

                    }
                });
            }
        };
    }



    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public String convertsecondintomin(int sec){

        int hour= sec/3600;
        int min= sec/60;
        while (min>=60) min=min-60;
        int second= sec % 60;

        return String.format("%02d", hour)+" : "+String.format("%02d", min)+" : "+String.format("%02d", second);
    }

   ////////////////////////////////

    public static String GET(String url) {
        InputStream inputStream = null;
        String result = "";
        try {

            // create HttpClient
            HttpClient httpclient = new DefaultHttpClient();

            // make GET request to the given URL
            HttpResponse httpResponse = httpclient.execute(new HttpGet(url));

            // receive response as inputStream
            inputStream = httpResponse.getEntity().getContent();

            // convert inputstream to string
            if (inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = String.valueOf(R.string.Did_not_work);

        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }

        return result;
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

    private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

            return GET(urls[0]);
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {


            //Toast.makeText(getBaseContext(), "Received!", Toast.LENGTH_LONG).show();


            //parsing the json received

            String data = "";

            try {
                startTimer();
                //Toast.makeText(getBaseContext(), "Receivedtry!", Toast.LENGTH_LONG).show();
                JSONObject json = new JSONObject(result);

                JSONArray path = json.getJSONArray("schedule");

                remaining = path.getJSONObject(0).getInt("timestamp");

                exacttime = path.getJSONObject(0).getString("exacttime");

                fare = path.getJSONObject(0).getString("fare");

                infodate = path.getJSONObject(0).getString("infodate");

                txtinfodate.setText(infodate);

                minuteremaining.setText(convertsecondintomin(remaining).toString());

                exact.setText(exacttime);
                displayfare.setText(fare);

                //exactime.setText(exacttime);

                JSONArray full = json.getJSONArray("fullschedule");
                fulltab = full.getJSONObject(0).getString("full");

                String[] exploded=fulltab.split("-");

                listItems.clear();

                for(int i = 0; i < exploded.length; i++){
                    listItems.add(exploded[i]);

                }

                //ada.notifyDataSetChanged();
                txtinfodate.setVisibility(View.VISIBLE);
                displayfare.setVisibility(View.VISIBLE);
                fullschedule.setAdapter(ada);
                headtitle.setVisibility(View.VISIBLE);
                at.setVisibility(View.VISIBLE);
                fullscheduleheader.setVisibility(View.VISIBLE);
                send.setEnabled(true);





            }
            catch (JSONException e) {
                e.printStackTrace();
            }

            //ada.notifyDataSetChanged();
            fullschedule.setAdapter(ada);
            send.setEnabled(true);

        }
    }

}
